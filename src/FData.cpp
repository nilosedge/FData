//============================================================================
// Name        : FData.cpp
// Author      : Olin
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <fstream>
#include <strstream>
#include <string>
#include <iostream>
#include <istream>
#include <ostream>
#include <sstream>
#include "RunningCalc.h"

using namespace std;

int main() {

	string line;
	ifstream file("/Volumes/ramdisk/USDJPY.csv");

	ofstream outfile("/Users/oblodgett/Desktop/ForexData/tick/csv/USDJPY/fulldata.tsv");

	string date;
	string ask;
	string bid;

	string s;

	long double askld;
	long double bidld;

	RunningCalc bidcalc10(10);
	RunningCalc bidcalc20(20);
	RunningCalc bidcalc50(50);
	RunningCalc bidcalc100(100);
	RunningCalc bidcalc200(200);

	RunningCalc askcalc10(10);
	RunningCalc askcalc20(20);
	RunningCalc askcalc50(50);
	RunningCalc askcalc100(100);
	RunningCalc askcalc200(200);

	int cnt = 1;
	while(getline(file, line)) {
		//cout << "Line: " << line << endl;
		istringstream ss(line);

		string s;

		int c = 0;
		while (getline(ss, s, ',')) {
			if(c == 0) date = s;
			if(c == 1) bid = s;
			if(c == 2) ask = s;
			c++;
		}


		date[12];

		istringstream askd(ask);
		askd >> askld;
		istringstream bidd(bid);
		bidd >> bidld;

		bidcalc10.add(bidld);
		bidcalc20.add(bidld);
		bidcalc50.add(bidld);
		bidcalc100.add(bidld);
		bidcalc200.add(bidld);

		askcalc10.add(askld);
		askcalc20.add(askld);
		askcalc50.add(askld);
		askcalc100.add(askld);
		askcalc200.add(askld);

		outfile.precision(8);

		outfile << fixed << cnt << "\t"
				<< date[0] << date[1] << date[2] << date[3] << "-" << date[4] << date[5] << "-" << date[6] << date[7] << " "
				<< date[9] << date[10] << ":" << date[11] << date[12] << ":" << date[13] << date[14] << "." << date[15] << date[16] << date[17]
				<< "\t" << bid
				<< "\t" << bidcalc10.getAvg()  << "\t" << bidcalc10.getStd()  << "\t" << bidcalc10.getPlus2()  << "\t" << bidcalc10.getMinus2()
				<< "\t" << bidcalc20.getAvg()  << "\t" << bidcalc20.getStd()  << "\t" << bidcalc20.getPlus2()  << "\t" << bidcalc20.getMinus2()
				<< "\t" << bidcalc50.getAvg()  << "\t" << bidcalc50.getStd()  << "\t" << bidcalc50.getPlus2()  << "\t" << bidcalc50.getMinus2()
				<< "\t" << bidcalc100.getAvg() << "\t" << bidcalc100.getStd() << "\t" << bidcalc100.getPlus2() << "\t" << bidcalc100.getMinus2()
				<< "\t" << bidcalc200.getAvg() << "\t" << bidcalc200.getStd() << "\t" << bidcalc200.getPlus2() << "\t" << bidcalc200.getMinus2()
				<< "\t" << ask
				<< "\t" << askcalc10.getAvg()  << "\t" << askcalc10.getStd()  << "\t" << askcalc10.getPlus2()  << "\t" << askcalc10.getMinus2()
				<< "\t" << askcalc20.getAvg()  << "\t" << askcalc20.getStd()  << "\t" << askcalc20.getPlus2()  << "\t" << askcalc20.getMinus2()
				<< "\t" << askcalc50.getAvg()  << "\t" << askcalc50.getStd()  << "\t" << askcalc50.getPlus2()  << "\t" << askcalc50.getMinus2()
				<< "\t" << askcalc100.getAvg() << "\t" << askcalc100.getStd() << "\t" << askcalc100.getPlus2() << "\t" << askcalc100.getMinus2()
				<< "\t" << askcalc200.getAvg() << "\t" << askcalc200.getStd() << "\t" << askcalc200.getPlus2() << "\t" << askcalc200.getMinus2() << "\n";

		if(cnt % 1000000 == 0 && cnt > 0) {
			cout << "Date: " << date << endl;
			//exit(0);
		}
		cnt++;
	}
	file.close();

}
