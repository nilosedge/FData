#include <iostream>
#include <math.h>
#include "RunningCalc.h"

using namespace std;

RunningCalc::RunningCalc(int size) {
	index = 0;
	SMA = 0;
	SQU = 0;
	avg = 0;
	std = 0;
	std2 = 0;
	sum = 0;
	plus2 = 0;
	minus2 = 0;
	this->size = size;
	list = new long double[size];
	for(int i = 0; i < size; i++) {
		list[i] = 0;
	}
}

void RunningCalc::add(long double in) {
	long double num = list[index];
	SMA -= num;
	SQU -= (num * num);
	list[index] = in;
	SMA += in;
	SQU += (in * in);
	avg = (SMA / size);
	std = sqrt((SQU - ((SMA * SMA) / size)) / size);
	std2 = (2 * std);
	++index %= size;
}

long double RunningCalc::getStd() {
	return std;
}

long double RunningCalc::getAvg() {
	return avg;
}

long double RunningCalc::getPlus2() {
	return avg + std2;
}

long double RunningCalc::getMinus2() {
	return avg - std2;
}

RunningCalc::~RunningCalc() {
	delete list;
}
