class RunningCalc {
	public:
		RunningCalc(int size);
		~RunningCalc();
		void add(long double in);
		long double getAvg();
		long double getStd();
		long double getPlus2();
		long double getMinus2();
	private:
		long double *list;
		unsigned int size;
		unsigned int index;
		long double SMA;
		long double SQU;
		long double avg;
		long double sum;
		long double std;
		long double std2;
		long double plus2;
		long double minus2;
};
